<?php

$curl=curl_init();
curl_setopt($curl, CURLOPT_URL, 'https://api.covid19api.com/summary');
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
$result = curl_exec($curl);
curl_close($curl);

$result=json_decode($result, true);
// var_dump($result);die;

// ===== Global setting
$NewConfirmed = $result['Global']['NewConfirmed'];
$TotalConfirmed = $result['Global']['TotalConfirmed'];
$NewDeaths = $result['Global']['NewDeaths'];
$TotalDeaths = $result['Global']['TotalDeaths'];
$NewRecovered = $result['Global']['NewRecovered'];
$TotalRecovered = $result['Global']['TotalRecovered'];

$countries = $result['Countries'];

    

    $tem_arr=[];
    foreach($countries as $key => $value){
        $tem_arr[]=$value['Country'];
    }

    
    $tem_con=[];
    foreach ($countries as $key => $value) {
        $tem_con[]=$value['TotalConfirmed'];
    };

    $tem_det=[];
    foreach ($countries as $key => $value) {
        $tem_det[]=$value['TotalDeaths'];
    };

    $tem_cover=[];
    foreach ($countries as $key => $value) {
        $tem_cover[]=$value['TotalRecovered'];
    };


    $tem_color=[];
    $color=function(){
        $r=rand(0,255);
        $g=rand(0,255);
        $b=rand(0,255);
        return "rgb(".$r.",".$g.",".$b.")";
    };

    foreach($countries as $k => $v){
        for($x=1; $x<=count($v); $x++){
            $tem_color[]=$color();
        }
    }
?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
    <title>Dashboard COVID-19</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js"
    integrity="sha512-d9xgZrVZpmmQlfonhQUvTR7lMPtO7NkZMkA0ABN3PHCbKA5nqylQ/yWlFAyY6hYgdF1Qh6nYiuADWwKB4C2WSw=="
    crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="css/style.css">
    <link rel="shortcut icon" href="images/covid19.png" type="image/x-icon">

    <!-- style css internal -->
    <style>
    body{
        background-color: #09092f;
        background-image: url("images/map.png");
        background-size:800px;
    }
    </style>
</head>
<body>
    <!-- navbar -->
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <nav class="navbar navbar-expand-lg navbar-light bg-dark">
                <img class="mt-2" src="images/covid19.png" alt="logo" width="40px" height="40px">
                    <a class="navbar-brand text-light mx-3">Coronavirus(Covid-19)</a>
                    <div class="collapse navbar-collapse" id="navbarNavAltMarkup"></div>
                    <div>
                        <p class="text-light pt-2">Globally,as of <?php echo $countries[0]['Date']; ?></p>            
                    </div>
                </nav>
            </div>
        </div>
    </div>
     
    <!-- data total -->
    <div class="container-fluid mt-4">
        <div class="row">

            <div class="col-md-3 text-center text-light">
                <div class="icon1">
                    <i class="fas fa-viruses"></i>
                </div>
                <h3 class="mt-3">Coronavirus Disease (COVID-19) Dashboard</h3>
            </div>

            <div class="col-md-3">
                <div class="card text-warning text-center border" style="width: 19rem; background-color:#0e0e3e;">
                    <div class="card-body">
                        <h6 class="card-title">New Confirmed</h6>
                        <h3><?php echo number_format($NewConfirmed); ?><h3>
                        <h5 class="card-title">Total Confirmed</h5>
                        <h2><?php echo number_format($TotalConfirmed); ?></h2>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="card text-success text-center border" style="width: 19rem; background-color:#0e0e3e;">
                    <div class="card-body">
                        <h6 class="card-title">New Recovered</h6>
                        <h3><?php echo number_format($NewRecovered); ?><h3>
                        <h5 class="card-title">Total Recovered</h5>
                        <h2><?php echo number_format($TotalRecovered); ?></h2>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="card text-danger text-center border" style="width: 19rem; background-color:#0e0e3e;">
                    <div class="card-body">
                        <h6 class="card-title">New Deaths</h6>
                        <h3><?php echo number_format($NewDeaths); ?><h3>
                        <h5 class="card-title">Total Deaths</h5>
                        <h2><?php echo number_format($TotalDeaths); ?></h2>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- content  -->
    <div class="container-fluid mt-3">
      <div class="row">

        <div class="col-md-9 border m-4">
            <h3 class="text-center text-light">Total Confirmed</h3>
            <canvas id="TotalConfirmed"></canvas>
        </div>

        <div class="col-md-2 border m-4">
            <div id="nav" class="card-header text-light"><h5>Country / Global</h5></div>
            <div id="scroll">
            <?php foreach($countries as $key=>$value):?>
            <ul class="navbar-nav mr-auto">
                <li class="nav-item dropdown p-1">
                    <a class="nav-link text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?php echo $value['Country'];?>
                    </a>
                <div id="dropdown" class="dropdown-menu p-1 border" aria-labelledby="navbarDropdown">
                    <p class="text-light">Country : <?php echo $value['Country'];?></p>
                    <p class="text-light">CountryCode : <?php echo $value['CountryCode'];?></p>
                    <p class="text-warning">NewConfirmed : <?php echo $value['NewConfirmed'];?></p>
                    <p class="text-warning">TotalConfirmed : <?php echo $value['TotalConfirmed'];?></p>
                    <p class="text-success" >NewRecovered : <?php echo $value['NewRecovered'];?></p>
                    <p class="text-success" >TotalRecovered : <?php echo $value['TotalRecovered'];?></p>
                    <p class="text-danger">NewDeath : <?php echo $value['NewDeaths'];?></p>
                    <p class="text-danger">TotalDeath : <?php echo $value['TotalDeaths'];?></p>
                </div>
                </li>
            </ul>
            <?php endforeach;?>
            </div>
        </div>

      </div>
    </div>

    <!-- data total -->
    <div class="container-fluid mt-3">
        <div class="row">

            <div class="col-12 col-sm-12 col-md-6 mt-3 border">
                <h3 class="text-center text-light p-3">Total Deaths</h3>
                <canvas id="TotalDeaths"></canvas>
                <h2 class="text-center text-danger p-4"><?php echo number_format($TotalDeaths); ?> <span>cases</span></h2>
            </div>

            <div class="col-12 col-sm-12 col-md-6 mt-3 border">
                <h3 class="text-center text-light p-3">Total Recovered</h3>
                <canvas id="TotalRecovered"></canvas>
                <h2 class="text-center text-success p-4"><?php echo number_format($TotalRecovered); ?> <span>cases</span></h2>
            </div>
            
        </div>
    </div>

    <!-- footer -->
    <div class="container-fluid mt-3">
        <div class="row">
            <div class="col-12 col-sm-12">
                <p class="text-light text-center">Munif Soleh || Dashboard Covid-19<br>
                &copy;2020</p>
            </div>
        </div>
    </div>

    
    <!-- script chart bar TotalConfirmed -->
    <script>
        var ctx = document.getElementById('TotalConfirmed').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: <?php echo json_encode($tem_arr); ?>,
                datasets: [{
                    label: '# of Votes',
                    data: <?php echo json_encode($tem_con); ?>,
                    backgroundColor: <?php echo json_encode($tem_color); ?>,
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    </script>

    <!-- script chart pie Deaths -->
    <script>
        var ctx = document.getElementById('TotalDeaths').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: <?php echo json_encode($tem_arr); ?>,
                datasets: [{
                    label: '# of Votes',
                    data: <?php echo json_encode($tem_det); ?>,
                    backgroundColor: <?php echo json_encode($tem_color); ?>,
                    borderWidth: 0
                }]
            },
            options: {
                responsive: true,
                legend:{
                    display:false,
                }
            }
        });
    </script>

    <!-- script chart pie Recovered -->
    <script>
        var ctx = document.getElementById('TotalRecovered').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: <?php echo json_encode($tem_arr); ?>,
                datasets: [{
                    label: '# of Votes',
                    data: <?php echo json_encode($tem_det); ?>,
                    backgroundColor: <?php echo json_encode($tem_color); ?>,
                    borderWidth: 0
                }]
            },
            options: {
                responsive: true,
                legend:{
                    display:false,
                }
            }
        });
    </script>
    
        
    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>
</html>